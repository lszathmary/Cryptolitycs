# Cryptolitycs

## Update

**This application no longer works, since the free public API that was provided by cryptocompare.com is no longer available.**

## Description

Cryptolitycs is a cryptocurrency tracker developed for the Android platform.
It gives you quick and easy access to information about cryptocurrencies like
Bitcoin and many others. 

<img src="showcase/showcase.png" width="800">

## Features

 * Detailed information on every coin such as: percent change, available supply,
 market cap, highest price, lowest price. 

 * With the help of the generated price charts, the user can track how the price 
 of a currencies has changed during the past 30 days.  
 
 * Choose from a selection of more than 4000 different coins, including 
 populare coins like Ethereum, Bitcoin, Ripple and many more.
 
 * Contains a usable converter, which allows multiple conversions at once and
 provides up to 8 decimal places when needed for the great accuracy. 

 * Shows the most recent cryptocurrency related news from different sources.

## Libraries

The charts in the application are generated with help of 
[MP Android Chart](https://github.com/PhilJay/MPAndroidChart) library.

## Data Source

The application obtains all the cryptocurrency related data from 
[CryptoCompare](https://www.cryptocompare.com/) using the API provided by them.
